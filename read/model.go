package read

import (
	"context"
	"permissions-system/generic"

	acl "github.com/ory/keto/proto/ory/keto/acl/v1alpha1"
)

func ListObjects(r acl.RelationTuple) ([]string, error) {

	client := acl.NewReadServiceClient(generic.COONREAD)

	res, err := client.ListRelationTuples(context.Background(), &acl.ListRelationTuplesRequest{
		Query: &acl.ListRelationTuplesRequest_Query{
			Namespace: r.Namespace,
			Relation:  r.Relation,
			Subject:   r.Subject,
		},
	})
	if err != nil {
		return nil, err
	}
	var resp []string
	for _, rt := range res.RelationTuples {

		resp = append(resp, rt.Object)
	}
	return resp, nil
}

func CheckTuple(r *acl.RelationTuple) bool {

	client := acl.NewCheckServiceClient(generic.COONREAD)

	res, err := client.Check(context.Background(), &acl.CheckRequest{
		Namespace: r.Namespace,
		Object:    r.Object,
		Relation:  r.Relation,
		Subject:   r.Subject,
	})
	if err != nil {
		panic(err.Error())
	}

	if res.Allowed {

		return true
	}
	return false
}
