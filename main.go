package main

import (
	"log"
	"net/http"
	"permissions-system/generic"
	"permissions-system/mail"
	"permissions-system/rest"
	"time"

	httptransport "github.com/go-kit/kit/transport/http"
)

func main() {

	generic.InitSettings()

	go func() {
		for range time.Tick(time.Duration(generic.COLLECTTIME) * time.Minute) {
			mail.CheckMail()
		}
	}()

	//REST
	lc := httptransport.NewServer(
		rest.MakeListCustomersEndpoint(),
		rest.DecodeListCustomersRequest,
		rest.EncodeResponse,
	)

	uc := httptransport.NewServer(
		rest.MakeUserCheckEndpoint(),
		rest.DecodeUserCheckRequest,
		rest.EncodeResponse,
	)

	http.Handle("/list-customers", lc)
	http.Handle("/list-customers/", lc)
	http.Handle("/check-user-access", uc)
	http.Handle("/check-user-access/", uc)

	log.Fatal(http.ListenAndServe(generic.RESTPORT, nil))
}
