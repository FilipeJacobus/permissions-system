package generic

import (
	"strconv"

	"github.com/prometheus/common/log"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
)

// InitSettings initialize the settings for the app
func InitSettings() {
	// DEBUG mode
	viper.SetDefault("DEBUG", false)

	viper.AutomaticEnv()

	viper.SetConfigName("permission-system") // name of config file (without extension)
	viper.SetConfigType("yaml")              // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath("/etc")

	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		log.Error(err)
	}

	rt := viper.GetStringMap("rest")
	ml := viper.GetStringMap("email")
	kt := viper.GetStringMap("keto")

	RESTPORT = ":" + strconv.Itoa(rt["port"].(int))
	MAILTLS = ml["tls"].(string)
	MAILUSER = ml["user"].(string)
	MAILPASS = ml["password"].(string)
	MAILSMTP = ml["smtp"].(string)
	COLLECTTIME = ml["collect-time"].(int)
	KETOREAD = kt["read"].(string)
	KETOWRITE = kt["write"].(string)

	COONWRITE, _ = conectWrite()
	COONREAD, _ = conectRead()

}

func conectRead() (*grpc.ClientConn, error) {
	conn, err := grpc.Dial(KETOREAD, grpc.WithInsecure())
	return conn, err
}
func conectWrite() (*grpc.ClientConn, error) {
	conn, err := grpc.Dial(KETOWRITE, grpc.WithInsecure())
	return conn, err
}
