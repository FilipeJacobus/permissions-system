package generic

import "google.golang.org/grpc"

var (
	RESTPORT, MAILTLS, MAILUSER, MAILPASS, MAILSMTP, KETOREAD, KETOWRITE string
	COLLECTTIME                                                          int
	COONWRITE                                                            *grpc.ClientConn
	COONREAD                                                             *grpc.ClientConn
)
