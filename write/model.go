package write

import (
	"context"
	"permissions-system/generic"

	acl "github.com/ory/keto/proto/ory/keto/acl/v1alpha1"
)

func CreateTuple(r *acl.RelationTuple) bool {

	client := acl.NewWriteServiceClient(generic.COONWRITE)

	_, err := client.TransactRelationTuples(context.Background(), &acl.TransactRelationTuplesRequest{
		RelationTupleDeltas: []*acl.RelationTupleDelta{
			{
				Action:        acl.RelationTupleDelta_INSERT,
				RelationTuple: r,
			},
		},
	})
	if err == nil {
		return true
	}

	return false
}
