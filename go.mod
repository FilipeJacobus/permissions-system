module permissions-system

go 1.15

require (
	github.com/donatj/wordwrap v0.1.0 // indirect
	github.com/emersion/go-imap v1.1.0
	github.com/emersion/go-message v0.14.1
	github.com/go-kit/kit v0.10.0
	github.com/ory/keto/proto/ory/keto/acl/v1alpha1 v0.0.0-20210511095113-e82429ede43c
	github.com/prometheus/common v0.7.0
	github.com/spf13/viper v1.7.1
	google.golang.org/grpc v1.37.0
)
