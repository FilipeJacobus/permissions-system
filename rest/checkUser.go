package rest

import (
	"context"
	"net/http"
	"permissions-system/read"

	"github.com/go-kit/kit/endpoint"
	acl "github.com/ory/keto/proto/ory/keto/acl/v1alpha1"
)

func DecodeUserCheckRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var t Transport
	t.Method = r.Method
	if r.URL != nil {

		user := r.URL.Query().Get("user")
		cid := r.URL.Query().Get("customerid")
		relation := r.URL.Query().Get("relation")

		var r acl.RelationTuple
		r.Namespace = "customers"
		r.Relation = relation
		r.Object = cid
		r.Subject = &acl.Subject{Ref: &acl.Subject_Id{
			Id: user,
		}}
		t.Data = r
	}

	return t, nil
}

func MakeUserCheckEndpoint() endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (v interface{}, err error) {
		req := request.(Transport)
		switch req.Method {
		case "OPTIONS":
			v = "OK"

		case "GET":
			rd := req.Data
			ct := &rd
			v = read.CheckTuple(ct)

			v = UcResponse{Access: v.(bool)}

		default:
			v = 404
		}

		if err != nil {
			return err, nil
		}
		return v, nil

	}
}
