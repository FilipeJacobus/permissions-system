package rest

import (
	"context"
	"encoding/json"
	"net/http"
)

func EncodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Authorization")
	if v, ok := response.(int); ok {
		if v == 404 {
			w.WriteHeader(404)
			response = "404 not found"
		}
		if v == 201 {
			w.WriteHeader(201)
		}
	}
	return json.NewEncoder(w).Encode(response)
}
