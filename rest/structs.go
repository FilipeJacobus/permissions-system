package rest

import (
	acl "github.com/ory/keto/proto/ory/keto/acl/v1alpha1"
)

type Transport struct {
	Method    string
	Data      acl.RelationTuple
	Parameter string
	Origin    string
}

type UcResponse struct {
	Access bool `json:"access"`
}
