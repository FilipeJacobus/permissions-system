package mail

import (
	"io/ioutil"
	"log"
	"net/smtp"
	"strings"

	"permissions-system/generic"
	"permissions-system/read"
	"permissions-system/write"

	"github.com/emersion/go-imap"
	"github.com/emersion/go-imap/client"
	"github.com/emersion/go-message/mail"
	acl "github.com/ory/keto/proto/ory/keto/acl/v1alpha1"
)

func CheckMail() {

	// Connect to server
	c, err := client.DialTLS(generic.MAILTLS, nil)
	if err != nil {
		log.Fatal(err)
	}

	// Don't forget to logout
	defer c.Logout()

	// Login
	if err := c.Login(generic.MAILUSER, generic.MAILPASS); err != nil {
		log.Fatal(err)
	}

	// List mailboxes
	mailboxes := make(chan *imap.MailboxInfo, 100)
	done := make(chan error, 1)
	go func() {
		done <- c.List("", "*", mailboxes)
	}()

	if err := <-done; err != nil {
		log.Fatal(err)
	}

	// Select INBOX
	mbox, err := c.Select("INBOX", false)
	if err != nil {
		log.Fatal(err)
	}

	// Get the last 100 messages
	from := uint32(1)
	to := mbox.Messages
	if mbox.Messages > 99 {
		// We're using unsigned integers here, only subtract if the result is > 0
		from = mbox.Messages - 99
	}
	seqset := new(imap.SeqSet)
	seqset.AddRange(from, to)

	done = make(chan error, 1)

	// Get the whole message body
	var section imap.BodySectionName
	section.FetchItem()
	items := []imap.FetchItem{section.FetchItem(), imap.FetchEnvelope}
	messages := make(chan *imap.Message, 100)
	go func() {
		done <- c.Fetch(seqset, items, messages)

	}()

	for msg := range messages {
		subject := strings.ToLower(msg.Envelope.Subject)
		subject = strings.ReplaceAll(subject, " ", "")

		if subject == "novousuario" || subject == "novousuário" {
			var sender string
			if len(msg.Envelope.Sender) >= 1 {
				sender = msg.Envelope.Sender[0].MailboxName + "@" + msg.Envelope.Sender[0].HostName
			}

			r := msg.GetBody(&section)
			if r == nil {
				log.Fatal("Server didn't returned message body")
			}

			mr, err := mail.CreateReader(r)
			if err != nil {
				log.Fatal(err)
			}

			p, err := mr.NextPart()

			b, _ := ioutil.ReadAll(p.Body)
			user, relation, ids := getUserAndIds(string(b))
			sendEmail := false
			for _, v := range ids {

				if !hasRegister(user, relation, v) {
					includeUser(user, relation, v)
					sendEmail = true
				}

			}
			if sendEmail {
				SendConfirmEmail(sender, user, relation, ids)
			}

		}
	}

	if err := <-done; err != nil {
		log.Fatal(err)
	}

}

func SendConfirmEmail(sender, user, relation string, ids []string) {
	// Choose auth method and set it up
	auth := smtp.PlainAuth("", generic.MAILUSER, generic.MAILPASS, generic.MAILSMTP)

	// Here we do it all: connect to our server, set up a message and send it
	to := []string{sender}
	msg := []byte("To: " + sender + "\r\n" +
		"Subject: Confirma\xE7\xE3o de cadastro\r\n" +
		"\r\n" +
		"Seu usu\xE1rio foi cadastrado com sucesso\r\n" +
		"Usu\xE1rio: " + user + "\r\n" +
		"Rela\xE7\xE3o: " + relation + "\r\n" +
		"Ids: " + strings.Join(ids, ", ") + "\r\n")
	err := smtp.SendMail(generic.MAILSMTP+":587", auth, generic.MAILUSER, to, msg)
	if err != nil {
		log.Fatal(err)
	}
}

func hasRegister(email, relation string, id string) bool {

	var rt acl.RelationTuple
	var r = &rt
	r.Namespace = "customers"
	r.Object = id
	r.Relation = relation
	r.Subject = &acl.Subject{Ref: &acl.Subject_Id{
		Id: email,
	}}

	return read.CheckTuple(r)

}

func includeUser(email, relation string, id string) {

	var rt acl.RelationTuple
	var r = &rt
	r.Namespace = "customers"
	r.Object = id
	r.Relation = relation
	r.Subject = &acl.Subject{Ref: &acl.Subject_Id{
		Id: email,
	}}

	write.CreateTuple(r)
}

func getUserAndIds(str string) (email, relation string, ids []string) {

	var lines []string
	splt := strings.Split(str, "\n")
	for _, v := range splt {
		if v != "" {
			lines = append(lines, v)
		}

	}

	for _, v := range lines {
		s := strings.Split(v, ":")

		if len(s) > 1 {

			key := strings.ToLower(s[0])
			key = strings.ReplaceAll(key, " ", "")
			key = strings.ReplaceAll(key, "\r", "")
			key = strings.ReplaceAll(key, "ç", "c")
			key = strings.ReplaceAll(key, "ã", "a")

			val := strings.ToLower(s[1])

			val = strings.ReplaceAll(val, " ", "")
			val = strings.ReplaceAll(val, "\r", "")

			switch key {
			case "email":
				val := strings.ReplaceAll(val, "[1]", "")
				email = val
			case "ids":
				ids = strings.Split(val, ",")
			case "relacao":
				relation = val
			}
		}
	}
	return

}
